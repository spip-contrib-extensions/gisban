<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'aucun_resultat' => 'Aucun r&#233;sultat',
	'label_filtre' => 'Filtre',
	'label_filtre_explication' => 'Limite les recherches dans la base BAN. Exemples : "Guadeloupe 971", "97160 Le Moule"',
	'titre_config' => 'Configuration de GIS BAN (Base d\'Adresse Nationale)'

);

?>